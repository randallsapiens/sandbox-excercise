var emailRegex = 	
/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
var app = new Vue({
    el: '#app',
    data: {
        submission: {
            firstName: "",
            lastName: "",
            emailAddress: "",
            inquiry: "",
            recommendations: [{
                text: ""
            }]
        },
        errors: {
            inquiry: false,
            recommendations: false
        },
        onSubmit: false
    },
    methods: {
        addRecommendation: function () {
            this.submission.recommendations.push({
                text: ""
            })
        },
        removeRecommendation: function(index){
            Vue.delete(this.submission.recommendations,index);
        },
        validateEmail: function() {
           if(this.onSubmit){
            return !emailRegex.test(this.submission.emailAddress)
           }
           return false;            
        },
        validateFields: function (e) {
            var result = true
            this.onSubmit = true
            if(this.validateEmail()){
                result = false
            }
            if(this.submission.inquiry == ""){
                this.errors.inquiry = true
                result = false
            }else{
                this.errors.inquiry = false
            }
            if(this.submission.recommendations[0].text == ""){
                this.errors.recommendations = true
                result = false
            }else{
                this.errors.recommendations = true
            }

            if(result == true){                
                this.errors.inquiry = false
                this.errors.recommendations = false
            }

            return result;            
        },
        submitForm: function(e){
            e.preventDefault()

            if(this.validateFields()){
                var data = this.submission
                this.$http.post("/api/submissions",data)
                .then(function (response){
                    window.location.href = "/submissions";
                })
            }
        }
    }
})