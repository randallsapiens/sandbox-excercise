var app = new Vue({
    el: '#app',
    data: {
        submissions: []
    },
    created: function () {
        this.$http.get("/api/submissions")
        .then(function (response){
            var submissions = JSON.parse(response.bodyText);
            this.submissions = submissions.sort(function (a, b) {
             if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
               return 1;
             }
             if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
               return -1;
             }
             return 0;
           });
        })
    },
    methods: {
        numberOfCharacters: function (value){
            if(value.length < 150) return "Less than 150 characters"
            else if(value.length > 150) return "Greater than 150 characters"
            else return "Equals to 150 characters"
        },
        formatDate: function(value){
            return value.substring(0, 10)
        },
        formatRecommendations: function(value){
            return value.map(e => e.text).join(" | ")
        }
    }
})