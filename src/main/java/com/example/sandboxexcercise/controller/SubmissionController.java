package com.example.sandboxexcercise.controller;

import com.example.sandboxexcercise.model.Recommendation;
import com.example.sandboxexcercise.model.Submission;
import com.example.sandboxexcercise.repository.RecommendationJpaRepository;
import com.example.sandboxexcercise.repository.SubmissionJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SubmissionController {

    @Autowired
    SubmissionJpaRepository submissionRepository;
    @Autowired
    RecommendationJpaRepository recommendationJpaRepository;

    @GetMapping("/submissions")
    public List<Submission> getAllSubmissions(){
        return submissionRepository.findAll();
    }

    @PostMapping("/submissions")
    public Submission createSubmission(@Valid @RequestBody Submission submission){
        Submission s = submissionRepository.save(submission);

        List<Recommendation> recommendations = s.getRecommendations();

        recommendations.forEach(r -> {
            r.setSubmission(s);
        });

        recommendationJpaRepository.saveAll(recommendations);

        return s;

    }
    
}
