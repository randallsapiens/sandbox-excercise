package com.example.sandboxexcercise.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RoutesController {

    @RequestMapping("/submissions")
    public String submissions() {
        return "submissions";
    }
}

