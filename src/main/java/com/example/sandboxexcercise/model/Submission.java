package com.example.sandboxexcercise.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "submissions")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"updatedAt"})

public class Submission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    private String firstName;
    private String lastName;
    @NotBlank
    private String emailAddress;
    @NotBlank
    @Column(columnDefinition="TEXT")
    private String inquiry;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    @OneToMany(mappedBy = "submission")
    private List<Recommendation> recommendations;

    public Submission(){
        this.firstName = "";
        this.lastName = "";
        this.emailAddress = "";
        this.inquiry = "";
    }

    public Submission(String firstName, String lastName, @NotBlank String emailAddress, @NotBlank
    String inquiry, List<Recommendation> recommendations) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.inquiry = inquiry;
        this.recommendations = recommendations;
    }

    /* Getters */

    public String getFirstName() {
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getEmailAddress(){
        return emailAddress;
    }

    public String getInquiry(){
        return inquiry;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public List<Recommendation> getRecommendations(){
        return recommendations;
    }


    /* Setters */

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void seLastName(String lastName){
        this.lastName = firstName;
    }

    public void setEmailAddress(String emailAddress){
        this.emailAddress = emailAddress;
    }

    public  void setInquiry(String inquiry){
        this.inquiry = inquiry;
    }

    public void setRecommendation(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }


}
