package com.example.sandboxexcercise.repository;

import com.example.sandboxexcercise.model.Recommendation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecommendationJpaRepository extends JpaRepository<Recommendation, Integer> {

}
