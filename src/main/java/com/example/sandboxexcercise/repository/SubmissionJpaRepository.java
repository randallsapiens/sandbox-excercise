package com.example.sandboxexcercise.repository;

import com.example.sandboxexcercise.model.Submission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubmissionJpaRepository extends JpaRepository <Submission, Integer>{

}
