package com.example.sandboxexcercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SandboxExcerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxExcerciseApplication.class, args);
	}
}
