# sandbox-excercise

## Technologies used:

- Spring boot framework
- Java Persistance API
- Vue.js javascript framework [Vue.js](https://vuejs.org/)
- Bulma css framework [bulma.io](https://bulma.io/)
- MySql5.7 database
- Jetty server
- Maven

### Prerequisites
Internet connection application uses cdn javascript sources
### Optional

Install maven plugin command line

### Run

Open your favorite Java IDE and run it or use maven

```
mvn spring-boot:run
```

Note: Be sure to set the src/main/java/com/example/sandboxexcercise/SandboxExcerciseApplication.java as the executable file of the project. 

### Package

```
mvn package
```

### Run war file
```
java -jar sandbox-excercise-1.0.0.war
```



